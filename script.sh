artifactory_url="http://localhost:8081/artifactory"

repo="libs-snapshot-local"
group_id="com/qaagility/"
artifact_id="CounterWebApp"

url=$artifactory_url/$repo/$group_id/$artifact_id

file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

BUILD_LATEST="$url/$version/${artifact_id}-$build.war"
echo "File Name  = " +$BUILD_LATEST

echo $BUILD_LATEST > filename.txt
